package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args){

        int[] primeNumber = new int[5];
        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;

        System.out.println("The first prime number is: " + primeNumber[0]);
        System.out.println("The Second prime number is: " + primeNumber[1]);
        System.out.println("The Third prime number is: " + primeNumber[2]);
        System.out.println("The Fourth prime number is: " + primeNumber[3]);
        System.out.println("The Fifth prime number is: " + primeNumber[4]);

        ArrayList<String> friend = new ArrayList<String>();

        friend.add("John");
        friend.add("Jane");
        friend.add("Chloe");
        friend.add("Zoey");

        System.out.println("My friends are: " + friend);

        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println(inventory);

    }
}
