package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
        //[SECTION] JAVA COLLECTION
            // are a single units of objects
            // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with the loops.
    public static void main(String[] args){
        //[SECTION] Arrays
            // in java arrays are containers of values of the same data type given a predefined amount of values.
            // java arrays are more rigid, once the size and data type are defined, they can no longer change.

        // Synthax: Array Declaration
            // dataType[] identifier = new dataType[numOfElements];



        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        // This will return the memory address of the array

        //to print the intArray, we need to import the "Arrays" class and use the .toString method to convert the array to string.

        System.out.println(Arrays.toString(intArray));

        // Synthax: Array Declaration with initialization
            //dataType[] identifier = {elementA, elementB, elementC...}

        String[] names = {"John", "Jane", "Joe"};
//        names[4] = "Joey"; out of bounce length error
        System.out.println(Arrays.toString(names));

        // sample array
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: " + Arrays.toString(intArray));

        // Multidimensional Arrays
            // A two-dimensional array can be described as two lengths of nested array within each other, like a matrix
                // first length is row and second length is column
            //

            // Synthax: dataType[][] identifier = new dataType[row length][col length]

        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";


        System.out.println(Arrays.deepToString(classroom));

        // ArrayLists
            // are resizeable array wherein elements can be added or removed whenever it is needed.
        //Synthax:
                // Arraylist<dataType> identier = new ArrayList<dataType>();

        // Declare an Arraylist

        ArrayList<String> students = new ArrayList<String>();

        // add element

        students.add("John");
        students.add("Paul");

        System.out.println(students);

        // access element;
        // arraylistName.get(index)

        // Arrays.asList(arrayList) - from array to arrayList

        System.out.println(students.get(1));

        //ArrayList<String> students = new ArraList<String>(Arrays.asList(names))
        //arrayListname.add([index],value);

        students.add(1, "Mike");
        System.out.println(students);

        // updating an element
        //arrayListname.set(index,element);
        students.set(1,"George");
        System.out.println(students);

        // remove an element
        //arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        //remove all elements
//        students.clear();
        System.out.println(students);

        System.out.println(students.size());

        //[SECTION] Hashmaps


        /*// most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
            // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
            // in Java "keys" also referred as "fields"
            // wherein the values are accessed by the fields
            // Syntax:
                // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();*/

//        add element
//            hasmapname.put(<field>, <value>);
        HashMap<String, String> jobPosition = new HashMap<String, String>();
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
//        jobPosition.put("Student", "John");

        //the last added element with the same field will overrided, whenever there are duplicate keys

        System.out.println(jobPosition);
        //access element
        //hashMapName.get("field");

        System.out.println(jobPosition.get("dreamer")); //case-sensitive (asking for field)

        //updating the values
        //hashMapName.replace(keyToChanged, newValue);
        jobPosition.replace("Student","Von");
        System.out.println(jobPosition);

        // remove an element
        // hashMapName.remove(key/field);

        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        // clear all the content
        //hashmapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);




    }
}
