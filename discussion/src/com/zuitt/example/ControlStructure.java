package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        // [SECTION] JAVA Operators
        //arithmethic -> +,-,*,/,%;
        // Comparisson -> >, <, >=, <= ==, !=
        //Logical -> &&, ||, !
        // Assignment -> =


        // [SECTION] Slection Control Structure in JAva
        //if else

        int num = 36;
        if(num % 5 == 0){
            System.out.println(num + " is Divisible by 5");
        }
        else {
            System.out.println(num + " is not Divisible by 5");

        }

        //[SECTION] Short Circuiting
        int x = 15;
        int y = 0;

        // a technique applicable only to the AND & OR operators wherein if-statements or other control structures can exit early by ensuring either safety of operation or efficiency.
        // right hand operand is not evaluated
        // OR operator
        // (true || ...) = true
        //AND operator
        // (false || ...) = false
        // This is helpful to prevent run time errors.

        if(y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);

        }


        //[SECTION] Ternary Operator

        int num1 = 24;
        Boolean result = (num1 > 0) ? true : false;
        System.out.println(result);


        //[SECTION] Switch Cases;

        Scanner numberScan = new Scanner(System.in);

        System.out.println("Enter Number: ");
        int directionValue = numberScan.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("East");
                break;
            default:
                System.out.println("Invalid Option");
        }



    }
}